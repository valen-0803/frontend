INSERT INTO categoría (nombre) VALUES
('accesorios industriales'),
('batas'), 
('calzado'),
('conjuntos'), 
('camisas'),
('chalecos'),
('chaquetas'), 
('gorros'),    
('overoles'), 
('pantalones'), 
('petos');

insert into estado (descripcion) values 
('SI'), 
('NO');

INSERT INTO usuario (nombre_usuario, contraseña, documento) VALUES
('dotaciones colombia', '1234', '39806307');

INSERT INTO proveedor (nombre_proveedor, NIT, direccion, telefono) VALUES
('DOTACIONES COLOMBIA EN DRILL', '39806307', 'CRA 63 # 18- 22', '3108554134'), 
('DOTACIONES RICO', '80441904', 'CRA 65 # 45- 4O', '2186543'), 
('DOTACIONES OLIMPICA', '47564675', 'CRA 63 # 67- 23', '7756435'), 
('CENTRAL DE DOTACIONES', '10125436', 'CRA 63 # 65- 02', '5673423'), 
('DOTACIONES EXITO', '15648795', 'CRA 68 # 17- 02', '1234567'), 
('DOTACIONES UNO A', '23456789', 'CRA 63 # 16- 45', '7654123'), 
('DOTACIONES CALDERON', '32154698', 'CRA 63 # 18- 15', '2233665'), 
('DOTACIONES ALCOSTO', '4568875', 'CRA 63 # 18- 22', '8974561'), 
('MUNDI DOTACIONES', '52136489', 'CRA 63 # 17- 22', '5673423'), 
('OVEROLES Y UNIFORMES EXPRESS', '69875644', 'CRA 68 # 22- 18', '1254879'), 
('DOTACIONES INDUSTRIALES ORION', '78945123', 'CRA 68 # 18- 25', '2135879'), 
('DELTA DOTACIONES', '84561234', 'CRA 63 # 18- 25', '2315469');

INSERT INTO producto (nombre, talla, color, precio_costo, precio_venta, cantidad, ganancia_costo_venta, id_categoria_pk, id_estado_pk, id_proveedores_pk) VALUES 
('casco proteccion', ' ', 'Rojo', '$ 2.500', '$ 5.000', '5', '$ 2.500', 1, 1, 5), 
('gafas anti-empañantes', " ",'negra', '$ 3.500', '$ 7.000', '15', '$ 3.500', 1, 1, 2), 
('overol piloto', '44', 'azul oscuro', '$ 40.000', '$ 45.000', ' 2 ',' $ 2.000', 9, 1, 1), 
('bota dieléctrica ',' 36 ',' $ 43.000 ',' $ 46.000 ',' 4 ',' $ 3.000', 3, 1, 6), 
('camisa oxford caballero', 'M ', ' negra ',' $ 39.000 ',' $ 42.000 ',' 5 ',' $ 3.000', 5, 1, 4), 
('camisa oxford dama ', ' L', 'morada ',' $ 38.000 ',' $ 40.000 ',' 6 ',' $ 2.000', 5, 1, 4), 
('overol drill dos piezas', '42', ' gris ' , '$ 38.000', '$ 40.000', '7', '$ 2.000', 9, 1, 12), 
('overol drill enterizo', '46', 'azul oscuro', '$ 35.000', '$ 45.000', '9', '$ 10.000', 9, 1, 1), 
('pantalon camuflado ', '40', 'beige ',' $ 15.000 ',' $ 25.000 ',' 4 ',' $ 10.000', 10, 1, 1), 
('camisa en drill ', '38', 'naranja ',' $ 15.000 ',' $ 25.000 ',' 3 ',' $ 10.000', 5, 1, 1), 
('cofia con vicera antifluido', ' ','transparente', '$ 20.000', '$ 25.000', '5', '$ 5.000', 8, 2, 3), 
('cofia con vicera dacron', ' ', 'blanco', '$ 20.000', '$ 25.000 ',' 4 ',' $ 5.000', 8, 1, 1), 
('pantalón antifluido ', 'M', ' blanco ',' $ 15.000 ',' $ 22.000 ',' 8 ',' $ 7.000',10, 1, 1), 
('camisa antifluido ', 'S', 'negro ',' $ 15.000 ',' $ 22.000 ',' 4 ' , '$ 7.000', 5, 1, 1), 
('conjunto antifluido', 'L', 'azul oscuro', '$ 25.000', '$ 35.000', '7', '$ 10.000', 4, 1, 1), 
('chaleco periodista', 'M', 'azul oscuro', '$ 25.000', '$ 35.000', '7', '$ 10.000', 6, 1, 1), 
('Jean dama licrado', '38', 'azul oscuro', ' $ 18.000 ',' $ 25.000 ',' 1 ',' $ 7.000', 10, 2, 3), 
('Jean caballero ', '40', 'negro ',' $ 20.000 ',' $ 25.000 ',' 1 ',' $ 10.000', 10, 2, 3), 
('chaqueta dama ', 'S', 'negro ',' $ 25.000 ',' $ 35.000 ',' 1 ',' $ 10.000', 7, 2, 4), 
('chaqueta hombre ', 'L', 'azul oscuro ',' $ 25.000 ',' $ 35.000 ',' 1 ',' $ 10.000', 7, 2, 4); 

INSERT INTO movimiento ( tipo_movimiento, fecha_movimiento, cantidad_movimiento, precio_unidad_movimiento, id_producto_pk, id_usuario_pk) VALUES
( "entrada",'2021-01-12', '5', '$ 2.500', 1, 1),
( "entrada",'2021-01-12', '15', '$ 2.500', 2, 1),
( "entrada",'2021-02-20', '2', ' $ 40.000', 3, 1),
( "entrada",'2021-02-23', '4', ' $ 43.000', 4, 1),
( "entrada",'2021-03-13', '5', '$ 39.000', 5, 1),
( "entrada",'2021-03-18', '6', '$ 38.000', 6, 1),
( "entrada",'2021-03-15', '7', '$ 38.000', 7, 1),
( "entrada",'2021-03-16', '9', '$ 38.000', 8, 1),
( "entrada",'2021-03-20', '4', '$ 40.000', 9, 1),
( "entrada",'2021-03-20', '3', '$ 30.000', 10, 1),
( "entrada",'2021-03-25', '2', '$ 15.000', 11, 1),
( "entrada",'2021-03-26', '5', '$ 20.000', 12, 1),
( "entrada",'2021-03-27', '4', '$ 20.000', 13, 1),
( "entrada",'2021-03-27', '2', '$ 28.000', 14, 1),
( "entrada",'2021-03-28', '8', '$ 15.000', 15, 1),
( "entrada",'2021-03-29', '4', '$ 16.000', 16, 1),
( "entrada",'2021-03-30', '7', '$ 2.000', 17,1),
( "entrada",'2021-04-02', '2', '$ 3.000', 18, 1),
( "entrada",'2021-04-02', '1', '$ 2.000', 19, 1),
( "entrada",'2021-04-02', '1', '$ 38.000', 19, 1);