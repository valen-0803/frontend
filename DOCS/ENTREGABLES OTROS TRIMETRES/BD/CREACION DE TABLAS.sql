drop database javinventory;
create database javinventory;
use javinventory;
create table if not exists  categoria 
(
 id_categoria int(10) primary key auto_increment,
 nombre varchar(20)
);
create table if not exists  estado 
(
 id_estado int(10) primary key auto_increment,
 descripción varchar(10)
);
create table if not exists  usuario
(
 id_usuario int(20) primary key auto_increment,
 nombre_usuario varchar(40),
 contraseña varchar(20),
 doumento varchar (15)
);
create table if not exists  proveedor 
(
 id_proveedor int(10) primary key auto_increment,
 nombre_proveedor varchar(40),
 NIT varchar (15),
 direccion varchar (20),
 telefono varchar (15)
);
create table if not exists  producto 
(
id_producto int(10) primary key auto_increment,
 nombre varchar(50),
 talla varchar (10),
 color varchar (15),
 precio_costo varchar(30),
 precio_venta varchar(30),
 cantidad varchar(20), 
 ganancia_costo_venta varchar (30),
 id_categoria int(10),
 constraint categoria_producto
 foreign key (id_categoria)
 references categoria (id_categoria),
 id_estado int(10),
 constraint estado_producto
 foreign key (id_estado)
 references estado (id_estado),
 id_proveedor int(10),
 constraint proveedor_producto
 foreign key (id_proveedor)
 references proveedor (id_proveedor)
);
create table if not exists  movimiento 
(
 id_movimiento int(10) primary key auto_increment,
 tipo_de_movimiento  varchar(50),
 fecha_movimiento varchar (15),
 cantidad_movimiento varchar (10),
 precio_unidad_movimiento varchar (30),
 id_producto int(10),
 constraint producto_movimiento
 foreign key (id_producto)
 references producto (id_producto),
 id_usuario int(10),
 constraint usuario_movimiento
 foreign key (id_usuario)
 references usuario (id_usuario)
);

